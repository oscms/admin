# Onboarding Package

> _Quick! I want to help! What’s the highest priority?_
1. **[Join our Slack](https://join.slack.com/t/oscmsbc/shared_invite/zt-ddr71ncg-JCYUe_nght5Dcr5_8LaZXw), join the conversation and claim an unclaimed task from the #help-leigh channel by starting a thread. Fulfill that task, and come back for more!***
2. Fill out your Slack profile, so we know how you can help.
Add yourself to the volunteer spreadsheet.
 Provide us with contact info, how many hours you’re willing to commit, and your area(s) of expertise
Introduce yourself on the #introductions channel.
Keep it brief: who you are, what you do, and how you can help.
If you have a technical background and you have more than 30 hours a week to contribute, please contact one of the primary contacts, slack admins, or the OSCMS director Leigh Christie (go to direct messages, click the ‘+’ sign and then search for Leigh, then click ‘start’ to start and conversation)  and let them know. We will arrange for a 1:1 with you to get you into a role that urgently needs to be filled.
Add a profile picture!
So we can put a face to the name!
Use threads to talk
Spread the word!
Share this document around! Friends, co-workers, whoever wants to help.
Join the [Facebook group!](https://www.facebook.com/groups/OSCMSBC/)
Invite people to it by clicking the “invite” button or by clicking on peoples names in the “invite” dialog box on the right hand side of your screen.
Invite people by sharing the group link on your Facebook timeline (as post)
Invite people by sharing the group link on large pages or groups that you are a member of
Blog about us too! Tell your parents, your neighbour and your dog that you are doing your part!
Read the rest of this document!
And as you do, a quick note about Google docs (and sheets). Many of the OSCMS-BC documents are shared by sharing links. When you connect that way, you will appear as Anonymous <Animal> (even though you may be logged in to your Google account). If you want to have your name show up so that we can all communicate better and see who is working where, invite yourself directly by email address. Click “Share”, enter your email under People, send the invite, then re-connect by the link you get in your email.

## What is OSCMS-BC?

### A Brief Introduction

[OSCMS](https://docs.google.com/document/d/1-71FJTmI1Q1kjSDLP0EegMERjg_0kk_7UfaRE4r66Mg/preview?fbclid=IwAR1kIl7Hr9-mSWFzva8MwYf5DFOvPjeE29HEQC9Ka10aYW-l28ePcFACW80#heading=h.6rcgzhjv3lfe) is an organization that aims to streamline the efforts of engineers, makers and manufacturers around the globe to fight the spread of COVID-19 and support our workers on the front lines.

[OSCMS-BC](https://docs.google.com/document/d/1WtzupJ4BtVFlYifkaMdRVDTZTJUSXgcp4Cu7AuGANd0/edit?usp=sharing) is our local chapter based in British Columbia, Canada. We are working closely with the Vancouver Makers for Emergency Response & Support group ([VMERS](https://www.facebook.com/groups/YVRmerHelp)) to support open-source COVID-19 initiatives based in BC.

**We aim to provide support to local and international efforts to fight COVID-19 by:**
1. Keeping track of all BC-based COVID-19 engineering, maker, hacker, and manufacturing projects. This includes corporate, non-corporate, government, and hospital-based initiatives.
2. Analyzing and understanding the most urgent and important engineering and manufacturing priorities/needs for our hospital, health and government partners.
3. Supporting all BC-based COVID-19 projects by:

    * Coordinating engineering and technical documentation volunteers based on need, and using OSCMS templates and documentation standards.

    * Sending all documentation to the global OSCMS organization so that our best projects can be used to save lives around the world.  

    * Providing other resources including:
        * Feedback from healthcare administration and physicians across BC
        * Introductions to material suppliers and manufacturers

    * Preventing redundancy and duplication of work through concise and clear documentation, keeping bureaucratic bloat to a minimum.

## Slack Usage

* The Slack should be used for communications of information and discussions related to the mission of OSCMSBC
* New users should use the “read first, ask first, reply later” approach
* Please read the channel’s description and make sure you are posting threads in the right place.
* Please use the style of older posts in data tracking and information tracking channels
* Channels can be created based on need. Channels should be created following the specified naming convention
* If users are a nuisance, they might be asked to be civil, otherwise talked to, moderated, or in worst case scenarios removed from the OSCMSBC Slack
* Use of threads is preferred over long discussions in channels
    * Got a question? Make a thread.
    * Got a discussion? Make a thread.

### There are two types of Channel per project category

#### Type 1: Team Channels

* #Team- Aerosol-Box-Jade: Used by teams to identify their own slack channel using memorable and differentiated names (Jade, Python, Daisy, Tropical)  make it easy to differentiate between project groups. Think of the names like meeting room names which make recall and navigation easier.

#### Type 2 General Project Category Channel:

* #P-Aerosol-Box :Used for discussions, documentations flagging and news. Main page for the project, used to track how teams are doing, if they need more support and if documentation is ready for sending to Mothership.

* Use threads in these general channe;s for everything else: thread for general discussions, threads for research discussions, threads for specific engineering or production problems and threads for general news on the design

### Making Personal Channel Collections in Slack

### Sections

* Slack allows you to create groups of channels and direct messages, and name the set whatever you like. For example, you can create a section for your team, department, or project, and put all the relevant conversations together.
* You can create a section by going to the ‘Channels’ header in the left sidebar and clicking on the three dots. Then choose ‘Create new section’ Enter a name for the section or pick one of the suggested names. You can optionally put an emoji of your choosing next to the section name, too. Click Create, and you're done.
* In addition to organizing your channels and messages with sections, you can also pare down how many items you see in your sidebar